package tech.jborn.shopify.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.jborn.shopify.db.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
