package tech.jborn.shopify.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class OrderItemRequest {
    @NotNull
    private Long productId;

    @NotNull
    private BigDecimal amount;
}
